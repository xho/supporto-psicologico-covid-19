# ATTENZIONE: questo repository è stato spostato

Abbiamo spostato il repository a questo indirizzo https://gitlab.com/bedita/covid19/supporto-psicologico-covid-19

Questo repository sarà presto archiviato e rimarrà solo in lettura.
Se vuoi contribuire al progetto lo puoi fare aprendo una [Merge Request](https://gitlab.com/bedita/covid19/supporto-psicologico-covid-19/-/merge_requests/new) 
o aprendo una [issue](https://gitlab.com/bedita/covid19/supporto-psicologico-covid-19/-/issues/new) sul [nuovo progetto](https://gitlab.com/bedita/covid19/supporto-psicologico-covid-19)